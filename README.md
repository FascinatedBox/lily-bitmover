lily-bitmover
=============

Usage: `lily src/run_bitmover.lily <input> [<command>...]`

Install: `garden install FascinatedBox/lily-bitmover`

This script contians a few basic ciphers. `input` is filtered through each
`command`, which can be one of the ciphers listed below. If a cipher is given
that is not listed below, it is ignored. Ciphers are case-sensitive.

Available ciphers are as follows:

### EachDecrease

Subtract 1 from every byte. If the byte is 0, it is set to 0xff.

### EachIncrease

Add 1 to every byte. If the byte is 0xff, it is set to 0.

### PairSwap

This walks through the input from left to right in pairs. Every pair of bytes is
swapped. If there are an odd number of bytes, the last byte is left alone.

### PairXor

This walks the input from left to right in pairs. With `left` as the first byte
in the pair and `right` as the second, this sets `right` to `left ^ right`.

### ShiftTotalLeft

This simulates a left shift over the whole of `input`. Upper bits from
right-ward bytes become low bits of left-ward bytes. The first bit of the first
value is sent to the last byte as the last value.

### ShiftTotalRight

This simulates a right shift over the whole of `input`. This does the reverse of
the above, moving low bits from left-ward bytes into high bits of right-ward
bytes. The last bit of the last byte carries over to be the high bit of the
first byte.
